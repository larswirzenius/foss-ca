---
title: "FOSS-CA"
subtitle: "Certifying signing keys for open source projects"
...

# Summary

A download tool that authenticates the file it downloads, for
downloading release artifacts from FOSS projects. Accompany it with a
"FOSS-CA" project to certify FOSS project signing keys.

# Example

As a person who needs to install Debian, Fedora, or other Linux
distribution, or Tails, I want to download the ISO file for installing
the system, and be sure it's valid: it's what the project published,
and it got transferred correctly.

# Motivation

Downloading is easy, but authenticating the download is quite tedious.
A tool to do that automatically would be a benefit to the world.

# Discussion

Authenticating a download can be done at various levels of confidence:

* high: verify an OpenPGP signature on the downloaded file, using a
  still-valid certificate that has been verified to be for the key the
  project uses to sign their release artifacts.
  - alternatively, verify that the downloaded file has a checksum that
    matches a SHA256SUMS.txt file from the publisher, and the checksum
    file is signed
* medium: same, but without verifying the certificate apart from
  downloading it from the project website
* low: same, but only checking a checksum, without verifying it with a
  signature

# Implementation, simplistic

Write a shell or Python script to download an ISO using curl and
verify its signature using sq against OpenPGP certificates in a
trusted collection.

This can be improved upon immensely, but it's the minimum viable
version that can't be pared down further.


# FOSS-CA

The big problem for the download tool described above is knowing what
OpenPGP certificate to trust for each project. I propose a new project
that provides a CA for FOSS project signing keys. I'm hazy on the
details, but suggest the following to start the discussion going:

* a group of people collaborating to be FOSS-CA together
  - might be volunteers, or paid to do this
* a git repository where the FOSS certificates are collected
  - each certificate is associated with a set of URL regexes for which
    it will be valid
  - this part is jointly managed by the people collaborating on FOSS-CA
  - controversial changes to these need to be handled by some process
* each collaborator may certify any of the certificates
  - certifications are added to the repository
  - the CA certifies not just the key, and its identity, but also the
    download URLs

The CA repo needs to be available to anyone who want to use it, and not
be tied to the download tool I proposed above. In fact, the download
tool is the less important part of this.


# Questions

* Do you think this is worth pursuing?
  - is it a distraction from Sequoia's goals?
* Do you have suggestions for how to implement any or all of this?
  - keep FOSS project OpenPGP certificates in a git repo, maintained
    in jointly, in public?
  - how can those certificates be "scoped"? Tails should not be able
    to sign a Debian release
  - have people act as CA independently to certify certificates, to
    avoid centralization?
  - how should certifications be distributed? in the git repo with the
    certificates?
* Are you willing to collaborate on this?
* What should "FOSS-CA" be called?

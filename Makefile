PANDOCOPTS = --standalone --number-sections --toc --toc-depth=2 "-Vdate=$(shell git describe)" --filter pandoc-filter-diagram
HTMLOPTS = -H foss-ca.css

.SUFFIXES: .md .html .pdf

.md.html:
	pandoc $(PANDOCOPTS) $(HTMLOPTS) --output $@ $<

.md.pdf:
	pandoc $(PANDOCOPTS) --output $@ $<

all: foss-ca.html foss-ca.pdf

foss-ca.html foss-ca.pdf: foss-ca.md foss-ca.css Makefile
